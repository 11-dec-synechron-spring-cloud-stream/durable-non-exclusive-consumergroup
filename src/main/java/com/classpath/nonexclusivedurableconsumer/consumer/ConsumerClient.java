package com.classpath.nonexclusivedurableconsumer.consumer;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.util.function.Consumer;

@Configuration
public class ConsumerClient {

    @Bean
    public Consumer<String> process(){
        return (data) -> {
            System.out.println("Processing the data :::: " + Thread.currentThread().getName());
            System.out.println(data.toUpperCase());
        };
    }
}