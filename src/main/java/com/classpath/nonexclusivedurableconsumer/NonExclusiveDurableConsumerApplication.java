package com.classpath.nonexclusivedurableconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NonExclusiveDurableConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(NonExclusiveDurableConsumerApplication.class, args);
    }

}
